OSLC Connect for Jira - DOORS sample add-in
-------------------------------------------

1) Overview

	OSLC Connect for Jira allows creating a link between a Jira issue and a DOORS object
	simply by drag and dropping the DOORS object from its module into the Jira link creation dialog.
	However, when multiple DOORS objects are selected in the module, only one of them is actually dropped in the Jira dialog,
	due to a limitation of the DOORS client itself.

	This sample add-in demonstrates how to workaround this limitation.
	The add-in contributes a "OSLC Connect" menu in the DOORS client, that enables copying all selected objects in the clipboard.
	User can then paste the clipboard content into the Jira link creation dialog to create at once a link for each selected object.
	
	This sample is fully functional and ready to use. You may use it as is in your DOORS client.
	It is expected that DOORS administrators would prefer integrating the functionality demonstrated in this sample in their own add-in,
	which they are free and welcome to do.

2) How to install

	Here are the steps to contribute the add-in to the DOORS client:
	
	- Copy the sample-addins folder in your file system and rename it to "addins".
	- Create a batch file to start the DOORS client with the following command line: 
	  		"%DOORSEXE%" -addins "%ADDINS%"
		where:
			- DOORSEXE is the absolute path to the doors.exe file,
			- ADDINS is the absolute path to the "addins" folder previously copied.

3) How to use
	
	Here are the steps to use the add-in:
	
	- In DOORS client:
		- Start the DOORS client with the contributed add-in, as explained in previous section.
		- Open a module in read-only mode, so that Jira is granted write access when creating links.
		- Select multiple objects in the module, by selecting a first object and use Shift+Click to select the last object. 
		- Click the "OSLC Connect -> Copy Selected Object Urls" menu in the menu bar of the module.
	- In Jira:
		- Navigate to a Jira issue.
		- Click "More > Link" menu, select "Collaboration Link", select a link type and the DOORS project and select "Drop existing" option.
		- Type Ctrl-V in the drop dialog to paste the copied URLs.
		- Click "Link" to create a link from the Jira issue to each pasted DOORS Object URL. 
