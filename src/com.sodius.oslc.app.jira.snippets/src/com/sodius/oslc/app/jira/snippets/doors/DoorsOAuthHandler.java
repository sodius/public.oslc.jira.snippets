package com.sodius.oslc.app.jira.snippets.doors;

import java.io.IOException;
import java.net.URI;

import com.sodius.oslc.client.oauth.OAuthHandler;
import com.sodius.oslc.core.model.RootServices;

import net.oauth.OAuthException;

/**
 * Handle user authorization for OAuth authentication with DOORS.
 */
class DoorsOAuthHandler implements OAuthHandler {

    @Override
    public void authorize(RootServices rootServices, URI authorizationUri) throws OAuthException {
        try {
            // The snippet is a command line tool, so the DOORS HTML form for user authorization can't be displayed here directly.
            // Let's ask the user to display the page in a browser, authenticate there, and resume the snippet when it's done.
            System.out.println();
            System.out.println("DOORS requires authentication in a web browser. Copy the following URL in a browser and log in DOORS:");
            System.out.println("Press ENTER when you are authenticated in the browser (at this stage you can close the browser page)");
            System.out.print("> ");
            System.out.println(authorizationUri);
            System.in.read();
            System.out.println("Authenticating to DOORS...");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public URI createCallback(RootServices rootServices, String verifierId) {
        // nothing, we are using a client tool, cannot get a ping by DOORS Web Access server when authentication is completed
        return null;
    }

    @Override
    public void downstreamAuthorize(URI authorizationUri) throws OAuthException {
        throw new OAuthException("Downstream authentcation is not used nor supported in this use case");
    }

}
