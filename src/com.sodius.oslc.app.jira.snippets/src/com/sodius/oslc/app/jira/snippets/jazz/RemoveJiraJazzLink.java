package com.sodius.oslc.app.jira.snippets.jazz;

import java.net.URI;

import org.eclipse.lyo.oslc4j.core.model.Link;

import com.sodius.oslc.app.jira.snippets.utils.JiraUris;
import com.sodius.oslc.app.jira.snippets.utils.LinkTypeUtils;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.core.process.links.model.DirectedLink;
import com.sodius.oslc.core.process.links.requests.RemoveLink;
import com.sodius.oslc.core.process.model.LinkType;
import com.sodius.oslc.domain.cm.model.OslcCm;
import com.sodius.oslc.domain.config.model.OslcConfig;

/**
 * Removes a link between a Jira issue and a Jazz artifact.
 *
 * <p>
 * The Jazz artifact is either:
 * <ul>
 * <li>a Change Request in /ccm</li>
 * <li>a Requirement in /rm</li>
 * <li>a Test artifact in /qm (Test Plan, Test Case, Test Execution Record, Test Case Result or Test Script)</li>
 * </ul>
 *
 * <h3>Arguments</h3>
 * <p>
 * This snippet expects the following ordered program arguments:
 * <ol>
 * <li>The URL of a Jira issue</li>
 * <li>The link type to remove (e.g. <code>IMPLEMENTS_REQUIREMENT</code>)</li>
 * <li>The URL of a Jazz artifact</li>
 * </ol>
 *
 * <p>
 * This snippet expects the following Java virtual machine arguments:
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jira.user</code> - user name to connect to Jira.</li>
 * <li><code>jira.password</code> - password of the specified Jira user.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * </ul>
 *
 * <p>
 * Java virtual machine arguments are set in the command line with:<br>
 * <code>-Dkey=value</code><br>
 * e.g.<br>
 * <code>-Djira.user=patricia</code>
 *
 * <h3>Jira Issue URL</h3>
 * The URL of the Jira issue is the web URL you see in your browser when displaying a issue, e.g.:<br>
 * <code>https://jira-server/browse/PRJ-1</code>
 *
 * <h3>Link Type</h3>
 * Here are the supported link types from a Jira issue to a Jazz artifact:
 * <ul>
 *
 * <li>Links to Change Requests (/ccm):
 * <ul>
 * <li>"Provides - Related Change Requests" association
 * <ul>
 * <li><code>AFFECTS_PLAN_ITEM</code></li>
 * <li><code>RELATED_CHANGE_REQUEST</code></li>
 * </ul>
 * </li>
 * </ul>
 * </li>
 *
 * <li>Links to Requirements (/rm):
 * <ul>
 * <li>"Provides - Implementation Requests" association
 * <ul>
 * <li><code>AFFECTS_REQUIREMENT</code></li>
 * <li><code>IMPLEMENTS_REQUIREMENT</code></li>
 * </ul>
 * </li>
 * <li>"Provides - Requirements Change Requests" association
 * <ul>
 * <li><code>TRACKS_REQUIREMENT</code></li>
 * </ul>
 * </li>
 * </ul>
 * </li>
 *
 * <li>Links to Test artifacts (/qm):
 * <ul>
 * <li>"Provides - Defects" association
 * <ul>
 * <li><code>AFFECTS_TEST_RESULT</code></li>
 * <li><code>BLOCKS_TEST_EXECUTION_RECORD</code></li>
 * <li><code>TESTED_BY_TEST_CASE</code></li>
 * </ul>
 * </li>
 * <li>"Provides - Quality Management Tasks" association
 * <ul>
 * <li><code>RELATED_TEST_PLAN</code></li>
 * <li><code>RELATED_TEST_CASE</code></li>
 * <li><code>RELATED_TEST_EXECUTION_RECORD</code></li>
 * <li><code>RELATED_TEST_SCRIPT</code></li>
 * </ul>
 * </li>
 * </ul>
 * </li>
 * </ul>
 *
 * <h3>Jazz Artifact URL</h3>
 * <p>
 * The URL of the Jazz artifact is the OSLC URL of the artifact, which you can obtain from the web page of the artifact:
 * <ul>
 * <li>In /ccm:
 * <ol>
 * <li>Navigate to the work item page.</li>
 * <li>Click the "Copy ID and Summary" button in the tool bar</li>
 * <li>Copy the URL of the hyperlink displayed in the dialog. This URL looks like: <br>
 * <code>https://jazz-server/ccm/resource/itemName/com.ibm.team.workitem.WorkItem/1</code></li>
 * </ol>
 * </li>
 * <li>In /rm:
 * <ol>
 * <li>Navigate to the requirement page.</li>
 * <li>Click the "More actions" button in the tool bar and click "Share Link to Artifact..."</li>
 * <li>Copy the URL displayed in the dialog. This URL looks like: <br>
 * <code>https://jazz-server/rm/resources/xxx?oslc_config.context=https://jazz-server/gc/configuration/1</code></li>
 * </ol>
 * </li>
 * <li>In /qm (versions 6.0.6.1 and higher):
 * <ol>
 * <li>Navigate to the test artifact page.</li>
 * <li>Click the "Copy link for this page" button in the tool bar</li>
 * <li>Click "Copy as an OSLC concept URL" and click "Copy OSLC URL". This URL looks like: <br>
 * <code>https://jazz-server/qm/oslc_qm/contexts/xxx/resources/com.ibm.rqm.planning.VersionedTestCase/_yyy?oslc_config.context=https://jazz-server/gc/configuration/1</code></li>
 * </ol>
 * </li>
 * </ul>
 */
public class RemoveJiraJazzLink extends JiraJazzSnippet {

    /*
     * See the class documentation for details on expected arguments
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 3) {
            throw new IllegalArgumentException("Expecting 3 program arguments");
        }

        // validate the Jira issue URL
        URI jiraIssueLocation = JiraUris.fromBrowserIssue(args[0]);

        // validate the link type
        LinkType linkType = LinkType.valueOf(args[1]);
        if (!linkType.getSourceRange().toString().equals(OslcCm.TYPE_CHANGE_REQUEST)) {
            throw new IllegalArgumentException("Link type " + linkType + " is not defined for a Jira issue but a " + linkType.getSourceRange());
        }

        // validate the Jazz artifact URL
        URI jazzArtifactLocation = URI.create(args[2]);

        // run the snippet
        new RemoveJiraJazzLink(jiraIssueLocation, linkType, jazzArtifactLocation).call();
    }

    private final URI jiraIssueLocation;
    private final LinkType linkType;
    private final URI jazzArtifactLocation;

    private RemoveJiraJazzLink(URI jiraIssueLocation, LinkType linkType, URI jazzArtifactLocation) {
        this.jiraIssueLocation = jiraIssueLocation;
        this.linkType = linkType;
        this.jazzArtifactLocation = jazzArtifactLocation;
    }

    @Override
    protected void run(OslcClient jiraClient, OslcClient jazzClient) throws Exception {

        // Remove the link from Jira to Jazz
        removeJiraToJazzLink(jiraClient);

        // Remove the back-link from Jazz to Jira
        removeJazzToJiraLink(jazzClient);

        System.out.println("Done.");
    }

    private void removeJiraToJazzLink(OslcClient client) {
        System.out.println("Removing '" + LinkTypeUtils.getTitle(linkType) + "' link from Jira to Jazz...");
        removeLink(client, jiraIssueLocation, linkType, jazzArtifactLocation);
    }

    private void removeJazzToJiraLink(OslcClient client) {

        // Ensure there is a back-link to remove.
        // Note this will always be the case for links between Jira and Jazz,
        // it's only between Architecture Resources and Requirements that there will not be any back-link to remove
        if (linkType.getBacklink().isPresent()) {
            LinkType backLinkType = linkType.getBacklink().get();
            System.out.println("Removing '" + LinkTypeUtils.getTitle(backLinkType) + "' back-link from Jazz to Jira...");

            // If the Jazz project is global configuration enabled, there won't be any back link to remove,
            // as incoming links will be discovered by Jazz.
            // The AddLink class will take care of adding the link only if necessary.
            removeLink(client, jazzArtifactLocation, backLinkType, jiraIssueLocation);
        }

        else {
            System.out.println("No back-link is to remove in Jazz");
        }
    }

    private void removeLink(OslcClient client, URI source, LinkType linkType, URI target) {
        URI unversionedTarget = OslcConfig.removeContext(target);
        DirectedLink link = new DirectedLink(linkType.getPropertyDefinition(), source, new Link(unversionedTarget));
        new RemoveLink(client, link).call();
    }
}
