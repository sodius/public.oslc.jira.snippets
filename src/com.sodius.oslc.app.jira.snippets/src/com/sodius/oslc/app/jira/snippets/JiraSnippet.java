package com.sodius.oslc.app.jira.snippets;

import java.util.concurrent.Callable;

import org.apache.http.auth.UsernamePasswordCredentials;

import com.sodius.oslc.client.ClientWebException;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.OslcClients;

/**
 * Base class for Jira snippets.
 *
 * <p>
 * This snippet expects the following Java virtual machine arguments:
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jira.user</code> - user name to connect to Jira.</li>
 * <li><code>jira.password</code> - password of the specified Jira user.</li>
 * </ul>
 */
public abstract class JiraSnippet implements Callable<Void> {

    /**
     * Creates an OSLC client to connect to Jira and calls the <code>run()</code> method.
     *
     * @see #run(OslcClient)
     */
    @Override
    public final Void call() throws Exception {
        try {
            OslcClient client = createJiraClient();
            run(client);
        } catch (ClientWebException e) {
            System.err.println("Failed to execute " + e.getRequest().getURI());
            System.err.println("Status " + e.getResponse().getStatusCode() + " - " + e.getResponse().getMessage());
            System.err.println("Entity: " + e.getResponse().getEntity(String.class));
        }

        return null;
    }

    /**
     * Sub-classes must implement what's to do when an OSLC client is available for Jira.
     */
    protected abstract void run(OslcClient client) throws Exception;

    /**
     * Creates an OSLC client to connect to a Jira environment.
     * The client uses BASIC authentication, which is made when the first request is executed on the server.
     * Client uses credentials specified with <code>jira.user</code> and <code>jira.password</code> Java virtual machine arguments.
     */
    private static OslcClient createJiraClient() {
        String userName = getRequiredProperty("jira.user");
        String password = getRequiredProperty("jira.password");
        return OslcClients.basic(new UsernamePasswordCredentials(userName, password)).create();
    }

    protected static String getRequiredProperty(String name) {
        String value = System.getProperty(name);
        if ((value == null) || value.isEmpty()) {
            throw new IllegalArgumentException("Missing required Java virtual machine argument: " + name);
        } else {
            return value;
        }
    }
}
