package com.sodius.oslc.app.jira.snippets.jazz;

import org.apache.http.auth.UsernamePasswordCredentials;

import com.sodius.oslc.app.jira.snippets.JiraSnippet;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.OslcClients;

/**
 * Base class for snippets interacting with both Jira and Jazz environments.
 *
 * <p>
 * This snippet expects the following Java virtual machine arguments:
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jira.user</code> - user name to connect to Jira.</li>
 * <li><code>jira.password</code> - password of the specified Jira user.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * </ul>
 *
 */
public abstract class JiraJazzSnippet extends JiraSnippet {

    /**
     * Creates a Jazz client and call the <code>run()</code> method with the two OSLC clients.
     *
     * @see #run(OslcClient, OslcClient)
     */
    @Override
    protected final void run(OslcClient jiraClient) throws Exception {
        OslcClient jazzClient = createJazzClient();
        run(jiraClient, jazzClient);
    }

    /**
     * Sub-classes must implement what's to do when OSLC clients are available for both environments.
     */
    protected abstract void run(OslcClient jiraClient, OslcClient jazzClient) throws Exception;

    /**
     * Creates an OSLC client to connect to a Jazz environment.
     * The client uses FORM based authentication, which is made when the first request is executed on the server.
     * Client uses credentials specified with <code>jazz.user</code> and <code>jazz.password</code> Java virtual machine arguments.
     */
    private static OslcClient createJazzClient() {
        String userName = getRequiredProperty("jazz.user");
        String password = getRequiredProperty("jazz.password");
        return OslcClients.jazzForm(new UsernamePasswordCredentials(userName, password)).create();
    }
}
