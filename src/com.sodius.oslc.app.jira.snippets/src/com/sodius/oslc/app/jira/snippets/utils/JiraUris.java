package com.sodius.oslc.app.jira.snippets.utils;

import java.net.URI;

/**
 * Provides facilities to get OSLC URLs based on Jira web page URLs.
 */
public class JiraUris {
    private static final String BROWSE_SEGMENT = "/browse/";

    /**
     * Returns the OSLC URL of a Jira issue based on the issue web page URL.
     * <p>
     * Converts those type of URLs:
     * <ul>
     * <li>https://server:8443/browse/ISSUE-ID</li>
     * <li>https://server:8443/CONTEXT/browse/ISSUE-ID</li>
     * </ul>
     *
     * to:
     * <ul>
     * <li>https://server:8443/rest/oslc/1.0/cm/issue/ISSUE-ID</li>
     * <li>https://server:8443/CONTEXT/rest/oslc/1.0/cm/issue/ISSUE-ID</li>
     * </ul>
     */
    public static URI fromBrowserIssue(String issueBrowserLocation) {

        // look for "/browse/"
        int i = issueBrowserLocation.indexOf(BROWSE_SEGMENT);
        if (i < 0) {
            throw new IllegalArgumentException("Unexpected Jira URL, segment not found: " + BROWSE_SEGMENT);
        }

        // extract base location (everything before browse segment, https://server:8443/CONTEXT/)
        String baseLocation = issueBrowserLocation.substring(0, i);

        // extract issue id
        i += BROWSE_SEGMENT.length();
        String issueId = issueBrowserLocation.substring(i);

        return URI.create(baseLocation + "/rest/oslc/1.0/cm/issue/" + issueId);
    }

    private JiraUris() {
    }

}
