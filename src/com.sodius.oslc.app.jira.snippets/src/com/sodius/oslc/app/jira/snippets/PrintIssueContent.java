package com.sodius.oslc.app.jira.snippets;

import java.net.URI;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.Occurs;
import org.eclipse.lyo.oslc4j.core.model.Property;
import org.eclipse.lyo.oslc4j.core.model.ResourceShape;
import org.eclipse.lyo.oslc4j.core.model.ValueType;

import com.google.common.base.Strings;
import com.sodius.oslc.app.jazz.model.JazzNetProcess;
import com.sodius.oslc.app.jira.snippets.utils.JiraUris;
import com.sodius.oslc.app.jira.snippets.utils.LinkTypeUtils;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResource;
import com.sodius.oslc.client.requests.GetResourceShape;
import com.sodius.oslc.core.acc.model.Acc;
import com.sodius.oslc.core.model.Dcterms;
import com.sodius.oslc.core.model.OslcCore;
import com.sodius.oslc.core.model.ResourceProperties;
import com.sodius.oslc.domain.cm.model.ChangeRequest;

/**
 * Prints all properties and links of a Jira issue.
 *
 * <h3>Arguments</h3>
 * <p>
 * This snippet expects the following ordered program arguments:
 * <ol>
 * <li>The URL of a Jira issue</li>
 * </ol>
 *
 * <p>
 * This snippet expects the following Java virtual machine arguments:
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jira.user</code> - user name to connect to Jira.</li>
 * <li><code>jira.password</code> - password of the specified Jira user.</li>
 * </ul>
 *
 * <p>
 * Java virtual machine arguments are set in the command line with:<br>
 * <code>-Dkey=value</code><br>
 * e.g.<br>
 * <code>-Djira.user=patricia</code>
 *
 * <h3>Jira Issue URL</h3>
 * The URL of the Jira issue is the web URL you see in your browser when displaying an issue, e.g.:<br>
 * <code>https://jira-server/browse/PRJ-1</code>
 */
public class PrintIssueContent extends JiraSnippet {

    /*
     * See the class documentation for details on expected arguments
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            throw new IllegalArgumentException("Expecting 1 program argument");
        }

        // validate the Jira issue URL
        URI issueLocation = JiraUris.fromBrowserIssue(args[0]);

        // run the snippet
        new PrintIssueContent(issueLocation).call();
    }

    private final URI issueLocation;

    private PrintIssueContent(URI issueLocation) {
        this.issueLocation = issueLocation;
    }

    @Override
    protected void run(OslcClient client) throws Exception {

        // read the issue
        ChangeRequest issue = readJiraIssue(client);

        // read the issue shape, describing available properties
        ResourceShape shape = readJiraIssueType(client, issue);

        // display properties
        printProperties(client, issue, shape);
    }

    private ChangeRequest readJiraIssue(OslcClient client) {
        System.out.println("Reading Jira issue...");
        ChangeRequest issue = new GetResource<>(client, issueLocation, ChangeRequest.class).get();
        return issue;
    }

    private ResourceShape readJiraIssueType(OslcClient client, ChangeRequest issue) {
        System.out.println("Reading Jira issue type...");
        ResourceShape shape = new GetResourceShape(client, issue.getInstanceShape()).get();
        return shape;
    }

    private void printProperties(OslcClient client, ChangeRequest issue, ResourceShape shape) {
        System.out.println("Issue properties:");
        List<Property> properties = getSortedProperties(shape);
        for (Property property : properties) {
            if (!FILTERED_PROPERTIES.contains(property.getPropertyDefinition())) {
                System.out.println();
                print(issue, property);
            }
        }
    }

    private void print(ChangeRequest requirement, Property property) {

        // title
        System.out.println(property.getTitle() + " <" + property.getPropertyDefinition() + '>');

        // type
        System.out.println("  type:  " + getDisplayType(property));

        // resource property?
        if (isResource(property)) {
            Collection<Link> links = ResourceProperties.getLinks(requirement, property);

            // a single link is expected?
            if (isOccurs(property, Occurs.ZeroOrOne) || isOccurs(property, Occurs.ExactlyOne)) {
                if (links.isEmpty()) {
                    System.out.println("  link:  <none>");
                } else {
                    System.out.println("  link:  " + getDisplayLink(links.iterator().next()));
                }
            }

            // many links
            else {
                if (links.isEmpty()) {
                    System.out.println("  links: <none>");
                } else {
                    System.out.println("  links:");
                    for (Link link : links) {
                        System.out.println("    - " + getDisplayLink(link));
                    }
                }
            }
        }

        // primitive property
        else {
            // Note: use getObject() as, for a print out, we don't care whether the primitive is a String, a date or anything else.
            Object value = ResourceProperties.getObject(requirement, property);
            if (value == null) {
                System.out.println("  value: <none>");
            } else if (value.getClass().isArray()) {
                System.out.println("  value: " + Arrays.asList((String[]) value));
            } else {
                System.out.println("  value: " + value);
            }
        }
    }

    private static List<Property> getSortedProperties(ResourceShape shape) {
        List<Property> properties = new ArrayList<>();
        for (Property property : shape.getProperties()) {

            // some properties have no title (projectArea, component, instanceShape) and are generally not of interest here
            if (!Strings.isNullOrEmpty(property.getTitle())) {
                properties.add(property);
            }
        }

        Collections.sort(properties, new Comparator<Property>() {
            @Override
            public int compare(Property o1, Property o2) {

                // put primitive properties before links
                if (isResource(o1)) {
                    if (!isResource(o2)) {
                        return 1;
                    }
                } else if (isResource(o2)) {
                    return -1;
                }

                // compare titles
                return Collator.getInstance().compare(o1.getTitle(), o2.getTitle());
            }
        });

        return properties;
    }

    private static String getDisplayType(Property property) {
        StringBuilder buffer = new StringBuilder();

        // type name
        buffer.append(getDisplayTypeName(property));

        // occurs
        String occurs = getDisplayOccurs(property);
        if (!Strings.isNullOrEmpty(occurs)) {
            buffer.append(' ');
            buffer.append(occurs);
        }

        return buffer.toString();
    }

    private static String getDisplayTypeName(Property property) {
        if (isType(property, ValueType.Boolean)) {
            return "boolean";
        } else if (isType(property, ValueType.Date)) {
            return "date";
        } else if (isType(property, ValueType.DateTime)) {
            return "datetime";
        } else if (isType(property, ValueType.Decimal)) {
            return "decimal";
        } else if (isType(property, ValueType.Double)) {
            return "double";
        } else if (isType(property, ValueType.Float)) {
            return "float";
        } else if (isType(property, ValueType.Integer)) {
            return "integer";
        } else if (isType(property, ValueType.String)) {
            return "string";
        } else if (isType(property, ValueType.XMLLiteral)) {
            return "xml literal";
        } else if (isResource(property)) {
            return getDisplayResourceRange(property);
        } else if (property.getValueType() == null) {
            return "<none>";
        } else {
            return property.getValueType().toString();
        }
    }

    private static String getDisplayResourceRange(Property property) {
        URI[] ranges = property.getRange();
        if ((ranges != null) && (ranges.length != 0)) {
            return LinkTypeUtils.getType(ranges[0]).getSimpleName();
        }

        return "resource";
    }

    /*
     * Note: some properties are not typed "Resource" but have a representation URI (inline or reference),
     * which indicates they are targeting a resource.
     */
    private static boolean isResource(Property property) {
        return isType(property, ValueType.Resource) || (property.getRepresentation() != null);
    }

    private static boolean isType(Property property, ValueType type) {
        return type.toString().equals(String.valueOf(property.getValueType()));
    }

    private static String getDisplayOccurs(Property property) {
        if (isOccurs(property, Occurs.ZeroOrMany)) {
            return "[0..*]";
        } else if (isOccurs(property, Occurs.OneOrMany)) {
            return "[1..*]";
        } else {
            return "";
        }
    }

    private static boolean isOccurs(Property property, Occurs occurs) {
        return occurs.toString().equals(String.valueOf(property.getOccurs()));
    }

    private static String getDisplayLink(Link link) {
        String label = link.getLabel();

        if (Strings.isNullOrEmpty(label)) {
            return "<" + link.getValue() + ">";
        } else {
            return label + " <" + link.getValue() + ">";
        }
    }

    // @formatter:off
    private static final Set<URI> FILTERED_PROPERTIES = new HashSet<>(Arrays.asList(new URI[] {
            URI.create(Acc.PROPERTY_ACCESS_CONTEXT),
            URI.create(Dcterms.PROPERTY_CONTRIBUTOR),
            URI.create(JazzNetProcess.PROPERTY_PROJECT_AREA),
            URI.create(OslcCore.PROPERTY_INSTANCE_SHAPE),
            URI.create(OslcCore.PROPERTY_SERVICE_PROVIDER),
    }));
    // @formatter:on
}
