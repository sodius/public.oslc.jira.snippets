package com.sodius.oslc.app.jira.snippets.doors;

import java.net.URI;

import org.eclipse.lyo.oslc4j.core.model.Link;

import com.sodius.oslc.app.jira.snippets.utils.JiraUris;
import com.sodius.oslc.app.jira.snippets.utils.LinkTypeUtils;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.core.process.links.model.DirectedLink;
import com.sodius.oslc.core.process.links.requests.RemoveLink;
import com.sodius.oslc.core.process.model.LinkType;
import com.sodius.oslc.domain.cm.model.OslcCm;

/**
 * Removes a link between a Jira issue and a DOORS requirement.
 *
 * <h3>Arguments</h3>
 * <p>
 * This snippet expects the following ordered program arguments:
 * <ol>
 * <li>The URL of a Jira issue</li>
 * <li>The link type to remove (e.g. <code>IMPLEMENTS_REQUIREMENT</code>)</li>
 * <li>The URL of a DOORS Web Access artifact (which uses a http or https protocol)</li>
 * </ol>
 *
 * <p>
 * This snippet expects the following Java virtual machine arguments:
 * <ul>
 * <li><code>sodius.license</code> - location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jira.user</code> - user name to connect to Jira.</li>
 * <li><code>jira.password</code> - password of the specified Jira user.</li>
 * <li><code>doors.rootservices</code> - rootservices location of DOORS Web Access.</li>
 * <li><code>doors.consumerKey</code> - consumer key for OAuth access with DOORS Web Access.</li>
 * <li><code>doors.consumerSecret</code> - consumer secret for OAuth access with DOORS Web Access.</li>
 * </ul>
 *
 * <p>
 * Java virtual machine arguments are set in the command line with:<br>
 * <code>-Dkey=value</code><br>
 * e.g.<br>
 * <code>-Djira.user=patricia</code>
 *
 * <h3>DOORS Authentication</h3>
 * DOORS Web Access only supports OAuth for a client application to connect to the server.
 * This snippet requires a consumer key/secret to be defined in DOORS for this program to be able to connect,
 * information to reference with <code>doors.consumerKey</code> and <code>doors.consumerSecret</code> Java virtual machine arguments.
 * <p>
 * When the snippet is executed, OAuth will require a user identity when connecting to DOORS.
 * This identity is usually obtained by displaying a DOORS HTML login page to the user.
 * As the snippet is more a command line tool, when DOORS authentiation is performed,
 * it requires the user to copy the DOORS login URL in a browser,
 * perform the login there and press ENTER in the Console to resume the snippet execution.
 *
 * <h3>Jira Issue URL</h3>
 * The URL of the Jira issue is the web URL you see in your browser when displaying a issue, e.g.:<br>
 * <code>https://jira-server/browse/PRJ-1</code>
 *
 * <h3>DOORS Requirement URL</h3>
 * The URL of the DOORS requirement is the web URL you see in the DOORS client when displaying the requirement properties, e.g.:<br>
 * <code>https://myDoorsServer:8443/dwa/rm/urn:rational::1-5cc706d77dbe579e-O-3-00000141</code><br>
 * If this URL starts with <code>doors://</code>, please contact your administrator,
 * it may mean DOORS Web Access is not configured for this database.
 *
 * <h3>Link Type</h3>
 * Here are the supported link types from a Jira issue to a DOORS artifact:
 * <ul>
 * <li>"Provides - Implementation Requests" association
 * <ul>
 * <li><code>AFFECTS_REQUIREMENT</code></li>
 * <li><code>IMPLEMENTS_REQUIREMENT</code></li>
 * </ul>
 * </li>
 * <li>"Provides - Requirements Change Requests" association
 * <ul>
 * <li><code>TRACKS_REQUIREMENT</code></li>
 * </ul>
 * </li>
 * </ul>
 */
public class RemoveJiraDoorsLink extends JiraDoorsSnippet {

    /*
     * See the class documentation for details on expected arguments
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 3) {
            throw new IllegalArgumentException("Expecting 3 program arguments");
        }

        // validate the Jira issue URL
        URI jiraIssueLocation = JiraUris.fromBrowserIssue(args[0]);

        // validate the link type
        LinkType linkType = LinkType.valueOf(args[1]);
        if (!linkType.getSourceRange().toString().equals(OslcCm.TYPE_CHANGE_REQUEST)) {
            throw new IllegalArgumentException("Link type " + linkType + " is not defined for a Jira issue but a " + linkType.getSourceRange());
        }

        // validate the Jazz artifact URL
        URI doorsArtifactLocation = URI.create(args[2]);

        // run the snippet
        new RemoveJiraDoorsLink(jiraIssueLocation, linkType, doorsArtifactLocation).call();
    }

    private final URI jiraIssueLocation;
    private final LinkType linkType;
    private final URI doorsArtifactLocation;

    private RemoveJiraDoorsLink(URI jiraIssueLocation, LinkType linkType, URI doorsArtifactLocation) {
        this.jiraIssueLocation = jiraIssueLocation;
        this.linkType = linkType;
        this.doorsArtifactLocation = doorsArtifactLocation;
    }

    @Override
    protected void run(OslcClient jiraClient, OslcClient doorsClient) throws Exception {

        // Remove the link from Jira to DOORS
        removeJiraToDoorsLink(jiraClient);

        // Remove the back-link from DOORS to Jira
        removeDoorsToJiraLink(doorsClient);

        System.out.println("Done.");
    }

    private void removeJiraToDoorsLink(OslcClient client) {
        System.out.println("Removing '" + LinkTypeUtils.getTitle(linkType) + "' link from Jira to DOORS...");
        removeLink(client, jiraIssueLocation, linkType, doorsArtifactLocation);
    }

    private void removeDoorsToJiraLink(OslcClient client) {

        // Ensure there is a back-link to remove.
        // Note this will always be the case for links between Jira and DOORS,
        // it's only between Architecture Resources and Requirements that there will not be any back-link to remove
        if (linkType.getBacklink().isPresent()) {
            LinkType backLinkType = linkType.getBacklink().get();
            System.out.println("Removing '" + LinkTypeUtils.getTitle(backLinkType) + "' back-link from DOORS to Jira...");

            removeLink(client, doorsArtifactLocation, backLinkType, jiraIssueLocation);
        }

        else {
            System.out.println("No back-link is to remove in DOORS");
        }
    }

    private void removeLink(OslcClient client, URI source, LinkType linkType, URI target) {
        DirectedLink link = new DirectedLink(linkType.getPropertyDefinition(), source, new Link(target));
        new RemoveLink(client, link).call();
    }
}
