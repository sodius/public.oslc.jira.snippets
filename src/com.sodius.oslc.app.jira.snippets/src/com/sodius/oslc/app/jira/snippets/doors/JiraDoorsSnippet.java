package com.sodius.oslc.app.jira.snippets.doors;

import java.net.URI;

import org.apache.http.HttpStatus;

import com.sodius.oslc.app.jira.snippets.JiraSnippet;
import com.sodius.oslc.client.ClientWebException;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.OslcClients;
import com.sodius.oslc.client.oauth.Friend;
import com.sodius.oslc.client.oauth.Friends;
import com.sodius.oslc.client.oauth.OAuthHandler;
import com.sodius.oslc.client.oauth.OAuthStore;

/**
 * Base class for snippets interacting with both Jira and DOORS.
 *
 * <p>
 * This snippet expects the following Java virtual machine arguments:
 * <ul>
 * <li><code>sodius.license</code> - location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jira.user</code> - user name to connect to Jira.</li>
 * <li><code>jira.password</code> - password of the specified Jira user.</li>
 * <li><code>doors.rootservices</code> - rootservices location of DOORS Web Access.</li>
 * <li><code>doors.consumerKey</code> - consumer key for OAuth access with DOORS Web Access.</li>
 * <li><code>doors.consumerSecret</code> - consumer secret for OAuth access with DOORS Web Access.</li>
 * </ul>
 *
 */
public abstract class JiraDoorsSnippet extends JiraSnippet {

    /**
     * Creates a Jazz client and call the <code>run()</code> method with the two OSLC clients.
     *
     * @see #run(OslcClient, OslcClient)
     */
    @Override
    protected final void run(OslcClient jiraClient) throws Exception {
        try {
            OslcClient doorsClient = createDoorsClient();
            run(jiraClient, doorsClient);
        } catch (ClientWebException e) {

            // Might fail to obtain exclusive edit on DOORS module
            if (HttpStatus.SC_CONFLICT == e.getResponse().getStatusCode()) {
                System.err.println("Failed to execute " + e.getRequest().getURI());
                System.err.println("DOORS module is currently being edited by another user");
                return;
            }

            throw e;
        }
    }

    /**
     * Sub-classes must implement what's to do when OSLC clients are available for both environments.
     */
    protected abstract void run(OslcClient jiraClient, OslcClient doorsClient) throws Exception;

    /**
     * Creates an OSLC client to connect to a DOORS Web Access environment.
     * The client uses OAuth authentication, which is made when the first request is executed on the server.
     */
    private static OslcClient createDoorsClient() {

        // create a friend that holds information to access DOORS
        URI rootservices = URI.create(getRequiredProperty("doors.rootservices"));
        String consumerKey = getRequiredProperty("doors.consumerKey");
        String consumerSecret = getRequiredProperty("doors.consumerSecret");
        Friend doorsFriend = Friends.createFriend(rootservices, consumerKey, consumerSecret);

        // create an OAuth store that keep track of OAuth tokens exchanged to access DOORS
        OAuthStore oauthStore = new OAuthStore(Friends.createFriendProvider(doorsFriend));
        OAuthHandler oauthHandler = new DoorsOAuthHandler();
        return OslcClients.oauth(oauthStore, oauthHandler).create();
    }
}
