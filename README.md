# OSLC Connect for Jira Snippets

## Resources for Integration in OSLC Applications

This repository holds resources to ease interactions with OSLC Connect for Jira from some OSLC applications.

### Integration in DOORS

A sample DOORS add-in is available for integration in IBM Engineering Requirements Management DOORS Family.
This sample enables copying all selected objects in a DOORS client into the clipboard, for then pasting the selection in OSLC Connect for Jira to create OSLC links.

The sample add-in is located here: [/tools/doors/sample-addins/](https://bitbucket.org/sodius/public.oslc.jira.snippets/tools/doors/sample-addins/)

A detailed description of the add-in is available here: [/tools/doors/sample-addins/readme.txt](https://bitbucket.org/sodius/public.oslc.jira.snippets/tools/doors/sample-addins/readme.txt)

## Resources for Java Developers

This repository holds resources for a Java developer to act as a client of OSLC Connect for Jira, to inspect Jira issues and create links.

### Preparing your Eclipse Platform

Eclipse is the recommended platform to execute the snippets.

Here are the necessary steps to have a working environment for the snippets: 

1. In Eclipse, import the [com.sodius.oslc.app.jira.snippets](https://bitbucket.org/sodius/public.oslc.jira.snippets/src/master/src/com.sodius.oslc.app.jira.snippets) project from the snippets repository. It is expected the code doesn't yet compile as the necessary libraries are not yet installed.
2. Select the **/com.sodius.oslc.app.jira.snippets/target/snippets.target** file and right-click **Open With > Target Editor**
6. Click **Set as Active Platform** on top right

At this stage the code should successfully compile in **com.sodius.oslc.app.jira.snippets** project.

### Obtaining a License

MDAccess for OSLC is license protected. Contact us to get a SodiusWillert license file to run the snippets

### Executing Snippets

This repository contains snippets demonstrating how to use an OSLC client code to query and update resources in a Jira application
and to create links to other OSLC application like the IBM Jazz platform.

Those snippets are located here: [/src/com.sodius.oslc.app.jira.snippets/src/com/sodius/oslc/app/jira/snippets/](https://bitbucket.org/sodius/public.oslc.jira.snippets/src/master/src/com.sodius.oslc.app.jira.snippets/src/com/sodius/oslc/app/jira/snippets/)

The Javadoc of each snippet details the expected program arguments and Java VM arguments for the snippet to run properly.

The snippets use MDAccess for OSLC to connect to Jira and other OSLC-enabled applications. 
You may refer to its [Developer Guide](https://help.sodius.cloud/help/topic/com.sodius.oslc.doc/html/overview.html) for more details.

### Snippet List

Jira snippets:

* [PrintIssueContent](https://bitbucket.org/sodius/public.oslc.jira.snippets/src/master/src/com.sodius.oslc.app.jira.snippets/src/com/sodius/oslc/app/jira/snippets/PrintIssueContent.java): prints all properties and links of a Jira issue.

Jira / Jazz snippets:

* [CreateJiraJazzLink](https://bitbucket.org/sodius/public.oslc.jira.snippets/src/master/src/com.sodius.oslc.app.jira.snippets/src/com/sodius/oslc/app/jira/snippets/jazz/CreateJiraJazzLink.java): creates a link between a Jira issue and a Jazz artifact
* [RemoveJiraJazzLink](https://bitbucket.org/sodius/public.oslc.jira.snippets/src/master/src/com.sodius.oslc.app.jira.snippets/src/com/sodius/oslc/app/jira/snippets/jazz/RemoveJiraJazzLink.java): removes a link between a Jira issue and a Jazz artifact

Jira / DOORS snippets:

* [CreateJiraDoorsLink](https://bitbucket.org/sodius/public.oslc.jira.snippets/src/master/src/com.sodius.oslc.app.jira.snippets/src/com/sodius/oslc/app/jira/snippets/doors/CreateJiraDoorsLink.java): creates a link between a Jira issue and a DOORS requirement
* [RemoveJiraDoorsLink](https://bitbucket.org/sodius/public.oslc.jira.snippets/src/master/src/com.sodius.oslc.app.jira.snippets/src/com/sodius/oslc/app/jira/snippets/doors/RemoveJiraDoorsLink.java): removes a link between a Jira issue and a DOORS requirement
